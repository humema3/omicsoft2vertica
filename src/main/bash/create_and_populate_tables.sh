#!/usr/bin/env bash

function joinArr {
    local d=$1
    shift
    echo -n "$1"
    shift
    printf "%s" "${@/#/$d}"
}

declare -a DATA_DIRS_AND_FILES
i=0
SQL_FILE=""
ROW_FRACTION=1
SCRATCH_DIR=""
SUCCESS_LOG_FILE=""
PASSWORD=""
DATASET=""
DB_USER=bioinfo_reader
while getopts ":d:o:r:s:f:p:a:u:" opt; do
    case $opt in
        d)
            DATA_DIRS_AND_FILES[$i]=$OPTARG
            i=$((i+1))
            ;;
        o)
            SQL_FILE=$OPTARG
            ;;
        r)
            ROW_FRACTION=$OPTARG
            ;;
        s)
            SCRATCH_DIR=$OPTARG
            ;;
        f)
            SUCCESS_LOG_FILE=$OPTARG
            ;;
        p)
            PASSWORD=$OPTARG
            ;;
        a)
            DATASET=$OPTARG
            ;;
        u)
            DB_USER=$OPTARG
            ;;
        :)
            echo "Option -$OPTARG requires an argument" >&2
            exit 1
            ;;
    esac
done
if [ -z $DATA_DIRS_AND_FILES ]; then
    echo "-d (DATA_DIRS_AND_FILES) argument not supplied"
    exit 1
fi
if [ -z $SQL_FILE ]; then
    echo "-o (SQL_FILE) argument not supplied"
    exit 1
fi
if [ -z $SUCCESS_LOG_FILE ]; then
    echo "-f (SUCCESS_LOG_FILE) argument not supplied"
    exit 1
fi
if [ -z $PASSWORD ]; then
    echo "-p (PASSWORD) argument not supplied"
    exit 1
fi
#if [ ! -z $SCRATCH_DIR ] && [ -d $SCRATCH_DIR ] && [ "`ls $SCRATCH_DIR | wc -l`" -gt "0" ]; then
#    echo "SCRATCH_DIR (-s) must be empty"
#    exit 1
#elif [ ! -d $SCRATCH_DIR ]; then
if [ ! -d $SCRATCH_DIR ]; then
    mkdir -p $SCRATCH_DIR
fi
if [ -z $DATASET ]; then
    DATASET=`basename $DATA_DIRS_AND_FILES`
    if [[ $DATASET =~ (.+)_current ]]; then
        DATASET=${BASH_REMATCH[1]}
    fi
    if [[ $DATASET =~ (.+)201. ]]; then # remove year
        DATASET=${BASH_REMATCH[1]}
    fi
fi

JAVA=/usr/prog/java/1.8.0_20/bin/java
SCRIPT_DIR=$(dirname `readlink -f $0`)
cd `dirname $SCRIPT_DIR`

CMD="./bash/create_table_schema.sh -k -d `joinArr \" -d \" ${DATA_DIRS_AND_FILES[@]}` -o $SQL_FILE -r $ROW_FRACTION -f $SUCCESS_LOG_FILE -a $DATASET"
if [ ! -z $SCRATCH_DIR ]; then
    CMD+=" -s $SCRATCH_DIR"
fi
echo $CMD >&2
$CMD
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    echo "Schema creation script exited with error" >&2
    exit $EXIT_STATUS
fi
declare -a SUCCESSFUL_DATA_FILES
mapfile -t SUCCESSFUL_DATA_FILES < $SUCCESS_LOG_FILE
if [ "${#SUCCESSFUL_DATA_FILES[@]}" -eq "0" ]; then
    echo "No successfully generated table schemas generated" >&2
    exit 1
fi
DATA_FILES_STR=`joinArr , "${SUCCESSFUL_DATA_FILES[@]}"`
declare -a TABLES
i=0
for DATA_FILE in ${SUCCESSFUL_DATA_FILES[@]}; do
    if [[ "`basename $DATA_FILE`" =~ (.+)\.txt ]]; then
        TABLES[$i]=${BASH_REMATCH[1]}
        i=$((i+1))
    fi
    perl -i -pe "s/NaN//g;" $DATA_FILE
done
TABLES_STR=`joinArr "," "${TABLES[@]}"`
CMD="$JAVA -jar ../../target/Omicsoft2Vertica-1.0-SNAPSHOT.jar -f $DATA_FILES_STR -t $TABLES_STR -u $DB_USER -p $PASSWORD -cf $SQL_FILE"
echo
echo $CMD >&2
$CMD