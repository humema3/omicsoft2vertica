#!/bin/bash

function makeDefaultScratchDir() {
    local CURRENT_DIR=`pwd`
    if [[ $CURRENT_DIR =~ /home/`whoami`/(.+) ]]; then
        SCRATCH_DIR=/scratch/`whoami`/${BASH_REMATCH[1]}
    else # just use first slash (after root) onward
        local INDEX=`expr index "${CURRENT_DIR:1}" /`
        local BASE=`echo ${CURRENT_DIR:$INDEX}`
        SCRATCH_DIR=/scratch/`whoami`/$BASE
    fi
    REMOVE_SCRATCH=0
    if [ ! -d $SCRATCH_DIR ]; then
        REMOVE_SCRATCH=1
    fi
    mkdir -p $SCRATCH_DIR
    local EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echo "Error making scratch directory"
        exit $EXIT_STATUS
    fi
    return 0
}

function processFile() {
    local FILE=$1

    local PYTHON=/home/humema3/anaconda3/bin/python
    local WORKING_FILE=$SCRATCH_DIR/temp.txt
    local MAX_NUM_ROWS_TO_USE_ALL=10000

    echo >&2
    echo $FILE >&2

    local ZIPPED_FILE
    local DELETE_UNZIPPED=0
    if [[ "`basename $FILE`" =~ (.+)\.gz ]]; then
        ZIPPED_FILE=$FILE
        local UNZIPPED_FILE=$SCRATCH_DIR/${DATASET}_${BASH_REMATCH[1]}
        if [ ! -f $UNZIPPED_FILE ]; then
            echo "Unzipping $FILE: `date +\"%F %T\"`" >&2
            zcat $FILE > $UNZIPPED_FILE
            if [ ! -z "$KEEP_UNZIPPED" ] && [ "$KEEP_UNZIPPED" -ne "1" ]; then
                DELETE_UNZIPPED=1
            fi
        fi
        FILE=$UNZIPPED_FILE
    fi
    local DELETE_WORKING=1
    if [ "`echo \"$ROW_FRACTION < 1\" | bc`" -eq "1" ]; then
        echo "Calculating number of rows: `date +\"%F %T\"`" >&2
        local NUM_ROWS=`wc -l $FILE | awk '{print $1}'`
        if [ "$NUM_ROWS" -gt "$MAX_NUM_ROWS_TO_USE_ALL" ]; then
            local NUM_ROWS_TO_USE=$(printf "%.0f" `echo "$ROW_FRACTION * $NUM_ROWS" | bc`)
            echo "Taking $NUM_ROWS_TO_USE / $NUM_ROWS rows: `date +\"%F %T\"`" >&2
            head -n $((NUM_ROWS_TO_USE+1)) $FILE > $WORKING_FILE
        else
            WORKING_FILE=$FILE
            DELETE_WORKING=0
        fi
    else
        WORKING_FILE=$FILE
        DELETE_WORKING=0
    fi

    local TABLE=""
    if [[ "`basename $FILE`" =~ (.+)\..+ ]]; then
        TABLE=${BASH_REMATCH[1]}
        if [[ $TABLE =~ ${DATASET}_(.+) ]]; then
            TABLE=${BASH_REMATCH[1]}
        fi
    else
        TABLE=$FILE
    fi

    echo "File: $FILE" >&2
    echo "Table: $TABLE" >&2
    echo >&2
    CMD="$PYTHON python/create_table_schema.py -f $WORKING_FILE -t $TABLE -d $DATASET"
    if [ ! -z $OUTPUT_FILE ]; then
        echo "$CMD >> $OUTPUT_FILE" >&2
        $CMD >> $OUTPUT_FILE
        EXIT_STATUS=$?
    else
        echo $CMD >&2
        $CMD
        EXIT_STATUS=$?
    fi

    if [ "$DELETE_WORKING" -eq "1" ]; then
        rm $WORKING_FILE
    fi

    if [ "$EXIT_STATUS" -ne "0" ]; then
        echo "Schema creation failed for $FILE with status $EXIT_STATUS" >&2
        FAILED_FILES[$FAILED_FILE_INDEX]=$FILE
        FAILED_FILE_INDEX=$((FAILED_FILE_INDEX+1))
        return $EXIT_STATUS
    fi

    if [ "$DELETE_UNZIPPED" -eq "1" ]; then
        echo "Deleting $FILE" >&2
        rm $FILE
        SUCCESSFUL_FILES[$SUCCESSFUL_FILE_INDEX]=$ZIPPED_FILE
    else
        SUCCESSFUL_FILES[$SUCCESSFUL_FILE_INDEX]=$FILE
    fi
    SUCCESSFUL_FILE_INDEX=$((SUCCESSFUL_FILE_INDEX+1))

    return 0
}

KEEP_UNZIPPED=0
OUTPUT_FILE=""
SCRATCH_DIR=""
declare -a DATA_FILES
FILE_COUNT=0
ROW_FRACTION=1
SUCCESS_LOG_FILE=""
DATASET=""
while getopts ":ko:d:s:r:f:a:" opt; do
    case $opt in
        k)
            KEEP_UNZIPPED=1
            ;;
        o)
            OUTPUT_FILE=$OPTARG
            ;;
        d)
            DATA=$OPTARG
            if [ -d $DATA ]; then
                PREV_FILE_COUNT=$FILE_COUNT
                for DATA_FILE in $DATA/*.txt*; do
                    DATA_FILES[$FILE_COUNT]=$DATA_FILE
                    FILE_COUNT=$((FILE_COUNT+1))
                done
                if [ "$PREV_FILE_COUNT" -eq "$FILE_COUNT" ]; then
                    echo "Warning: no valid files (.txt[.gz]) found in directory $DATA" >&2
                fi
            else
                DATA_FILES[$FILE_COUNT]=$OPTARG
                FILE_COUNT=$((FILE_COUNT+1))
            fi
            ;;
        s)
            SCRATCH_DIR=$OPTARG
            ;;
        r)
            ROW_FRACTION=$OPTARG
            if [ "`echo \"$ROW_FRACTION <= 0 || $ROW_FRACTION > 1\" | bc`" -eq "1" ]; then
                echo "Row fraction must be > 0, <= 1"
                exit 1
            fi
            ;;
        f)
            SUCCESS_LOG_FILE=$OPTARG
            ;;
        a)
            DATASET=$OPTARG
            ;;
        :)
            echo "Option -$OPTARG requires an argument" >&2
            exit 1
            ;;
    esac
done
if [ "${#DATA_FILES[@]}" -eq "0" ]; then
    echo "Could not find any appropriate data files" >&2
    exit 1
fi
echo "Data files: ${DATA_FILES[@]}" >&2
if [ -z $DATASET ]; then
    DATASET=$(basename `dirname ${DATA_FILES[0]}`)
    for FILE in ${DATA_FILES[@]}; do
        if [ "$(basename `dirname $FILE`)" != "$DATASET" ]; then
            echo "Conflicting datasets: $(basename `dirname $FILE`), $DATASET" >&2
            exit 1
        fi
    done
    if [[ $DATASET =~ (.+)_current ]]; then
        DATASET=${BASH_REMATCH[1]}
    fi
fi
echo "Dataset: $DATASET" >&2

SCRIPT_DIR=$(dirname `readlink -f $0`)
cd `dirname $SCRIPT_DIR`

if [ -z $SCRATCH_DIR ]; then
    makeDefaultScratchDir
fi

declare -a SUCCESSFUL_FILES
SUCCESSFUL_FILE_INDEX=0
declare -a FAILED_FILES
FAILED_FILE_INDEX=0
for FILE in ${DATA_FILES[@]}; do
    processFile $FILE
done

if [ "$KEEP_UNZIPPED" -eq "0" ] && [ "$REMOVE_SCRATCH" -eq "1" ]; then
    rm -r $SCRATCH_DIR
fi

if [ "${#SUCCESSFUL_FILES[@]}" -gt "0" ]; then
    echo
    if [ ! -z $SUCCESS_LOG_FILE ]; then
        echo "Files with successfully created schemas being written to $SUCCESS_LOG_FILE" >&2
        for FILE in ${SUCCESSFUL_FILES[@]}; do
            echo $FILE >> $SUCCESS_LOG_FILE
        done
    else
        echo "Files with successfully created schemas:" >&2
        for FILE in ${SUCCESSFUL_FILES[@]}; do
            echo $FILE >&2
        done
    fi

    if [ "${#FAILED_FILES[@]}" -gt "0" ]; then
        echo "Files for which schemas could not be created:" >&2
        for FILE in ${FAILED_FILES[@]}; do
            echo $FILE >&2
        done
    fi
    exit 0
else
    echo "No table schemas were successfully generated" >&2
    exit 1
fi