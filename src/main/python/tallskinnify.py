import sys

def main(argv):
    inputFile = argv[0]
    outputFile = argv[1]
    f1 = open(inputFile, 'r')
    f2 = open(outputFile, 'w')
    colNames = f1.readline().strip().split("\t")
    for line in f1:
        fields = line.strip().split("\t")
        rowName = fields[0]
        rowValues = fields[1:]
        assert len(rowValues) == len(colNames)
        for i in range(len(rowValues)):
            f2.write(rowName + "\t" + colNames[i] + "\t" + rowValues[i] + "\n")
    f1.close()
    f2.close()

if __name__ == "__main__":
    main(sys.argv[1:])