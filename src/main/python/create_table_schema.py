import math
import os
import pandas as pd
import re
import sys
from argparse import ArgumentParser
from datetime import datetime
from os.path import dirname

def createDataFromFile(file):
    df = pd.read_csv(file, sep="\t")
    # any processing here?
    return df

def getPrecisionAndScale(floatData):
    maxNumLeftDigits = 1
    maxNumRightDigits = 1
    for num in floatData:
        if num is None or math.isnan(num):
            continue
        m = re.match(r'-?(\d+)(\.(\d+))?$', str(num))
        if m is not None:
            leftOfDecimal = m.group(1)
            rightOfDecimal = m.group(3)
        else: # assume scientific notation
            m = re.match(r'-?\d(?:\.(\d+))?e-(\d+)', str(num))
            if m is not None:
                leftOfDecimal = '0'
                rightOfDecimal = int(m.group(2))
                if m.group(1) is not None:
                    rightOfDecimal += len(m.group(1))
                rightOfDecimal = str(rightOfDecimal)
            else:
                raise AssertionError("Unrecognized number format: " + str(num))

        numRightDigits = len(rightOfDecimal) if rightOfDecimal is not None else 1
        numLeftDigits = len(leftOfDecimal)
        if numLeftDigits > maxNumLeftDigits:
            maxNumLeftDigits = numLeftDigits
        if numRightDigits > maxNumRightDigits:
            maxNumRightDigits = numRightDigits
    return (maxNumLeftDigits + maxNumRightDigits, maxNumRightDigits)

def getVerticaType(dataColumn, precisionPadding):
    if dataColumn.name.startswith('Chromosome'): # usually comes out as integer inappropriately
        vtype = 'VARCHAR(2)'
    else:
        pd2vertica = {'int': 'INTEGER', 'object': 'VARCHAR', 'float': 'NUMBER', 'bool': 'BOOLEAN'}
        dtype = str(dataColumn.dtype)
        vtype = None
        for key in pd2vertica:
            if dtype.startswith(key):
                vtype = pd2vertica[key]
        if vtype is None:
            raise KeyError("Could not map " + dtype + " to a Vertica type")
        if vtype == 'VARCHAR':
            maxLength = max(dataColumn.astype(str).apply(len))
            vtype += '(' + str(maxLength) + ')'
        elif vtype == 'NUMBER':
            precision, scale = getPrecisionAndScale(dataColumn)
            vtype += '(' + str(precision + precisionPadding) + ',' + str(scale) + ')' # add extra digits just in case not all the rows fit the sample we tested
    isNumeric = vtype.startswith('INTEGER') or vtype.startswith('NUMBER')
    if isNumeric:
        isNullFunction = lambda x: x is None or re.match(r'\s*$', str(x)) is not None or math.isnan(x)
    else:
        isNullFunction = lambda x: x is None or re.match(r'\s*$', str(x)) is not None
    if sum(dataColumn.apply(isNullFunction)) == 0:
        vtype += ' NOT NULL'
    return vtype

def currentTime():
    return datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')

def main(argv):
    parser = ArgumentParser()
    parser.add_argument('-f', '--flat_file', dest='flat_file', type=str, required=True)
    parser.add_argument('-s', '--schema_name', dest='schema_name', type=str, required=False, default='omicsoft')
    parser.add_argument('-t', '--table_name', dest='table_name', type=str, required=False)
    parser.add_argument('-d', '--dataset', dest='dataset', type=str, required=True)
    parser.add_argument('-o', '--out_file', dest='out_file', type=str, required=False)
    parser.add_argument('--overwrite', dest='overwrite', default=False, action='store_true')
    parser.add_argument('--pad_precision', dest='pad_precision', type=int, required=False, default=3)
    args = parser.parse_args(argv)
    flatFile = args.flat_file
    schemaName = args.schema_name
    tableName = args.table_name
    dataset = args.dataset
    outputFile = args.out_file
    overwrite = args.overwrite
    padPrecision = args.pad_precision
    if tableName is None:
        tableName = os.path.basename(os.path.splitext(flatFile)[0])
    # strip dataset from table name to avoid redundancy
    m = re.match(dataset + r'_(.+)', tableName)
    if m is not None:
        tableName = m.group(1)
    sys.stderr.write("Reading file into data frame: " + currentTime() + "\n")
    data = createDataFromFile(flatFile)
    sys.stderr.write("Creating schema: " + currentTime() + "\n")
    fullTable = schemaName + '."' + dataset + '_' + tableName + '"'
    schema = ['DROP TABLE IF EXISTS ' + fullTable + ';', 'CREATE TABLE ' + fullTable + ' (']
    numCols = len(data.columns)
    i = 0
    for col in data.columns:
        i += 1
        sys.stderr.write('Processing column ' + col + ' (' + str(i) + '/' + str(numCols) + '): ' + currentTime() + "\n")
        vtype = getVerticaType(data[col], padPrecision)
        schema.append("\t" + '"' + col + '" ' + vtype + ",")
    schema = "\n".join(schema)
    schema = schema[:-1] + "\n);\n\n"
    if outputFile is not None:
        sys.stderr.write("Writing to " + outputFile + ": " + currentTime() + "\n")
        o = open(outputFile, 'w' if overwrite else 'a')
        o.write(schema)
        o.close()
    else:
        print(schema)


if __name__ == "__main__":
    main(sys.argv[1:])