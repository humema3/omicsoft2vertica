package com.novartis.nibr.oncology.omicsoft2vertica;

import org.apache.commons.cli.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by humema3 on 6/23/16.
 */
public class TPMTableCreator {


    public static void main(final String[] args) throws ParseException, SQLException {
        final Options options = getOptions();
        final CommandLineParser parser = new DefaultParser();
        final CommandLine cmd = parser.parse(options, args);

        final String schema = cmd.getOptionValue('s', "omicsoft");
        final String fpkmTable = schema + ".\""+ cmd.getOptionValue('f') + "\"";
        final String transcriptAnnotationTable = schema + ".\"" + cmd.getOptionValue('a') + "\"";
        final String url = cmd.getOptionValue("url", "jdbc:vertica://cam-prod-vertica12.nibr.novartis.net:5433/v_nibr_prod");
        final String user = cmd.getOptionValue('u');
        final String password = cmd.getOptionValue('p');
        final String sqlFile = cmd.getOptionValue("sql", "tpm.sql");
        final boolean keepSql = cmd.hasOption('k');

        final Properties connectionProps = new Properties();
        connectionProps.put("user", user);
        connectionProps.put("password", password);
        final Connection conn = DriverManager.getConnection(url, connectionProps);

        conn.close();

    }

    private static Options getOptions() {
        final Options options = new Options();

        options.addOption("f", "FPKM", true, "Table containing FPKM values");
        options.getOption("f").setRequired(true);

        options.addOption("a", "TranscriptAnnotation", true, "Table containing transcript annotation information");
        options.getOption("a").setRequired(true);

        options.addOption("o", "output", true, "SQL output file");
        options.getOption("o").setRequired(false);

        options.addOption("s", "schema", true, "Database schema to use");
        options.getOption("s").setRequired(false);

        options.addOption("u", "user", true, "Database user");
        options.getOption("u").setRequired(true);

        options.addOption("p", "password", true, "Database password");
        options.getOption("p").setRequired(true);

        options.addOption("url", "vertica_url", true, "URL of Vertica database to use");
        options.getOption("url").setRequired(false);

        options.addOption("sql", "sql_file", true, "File to use to generate SQL");
        options.getOption("sql").setRequired(false);

        options.addOption("k", "keep_sql", false, "Keep create table SQL file");
        options.getOption("k").setRequired(false);

        return options;
    }
}
