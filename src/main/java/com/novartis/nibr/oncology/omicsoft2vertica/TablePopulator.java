package com.novartis.nibr.oncology.omicsoft2vertica;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.vertica.jdbc.VerticaConnection;
import com.vertica.jdbc.VerticaCopyStream;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

public class TablePopulator {
//    private static final Logger LOGGER = LogManager.getLogger(TablePopulator.class);

    private enum VerticaType { VARCHAR, INTEGER, NUMERIC, BOOLEAN }

    private static class Column {
        private final String name;
        private final VerticaType type;
        private Column(final String name, final VerticaType type) {
            this.name = name;
            this.type = type;
        }
        private String getName() {
            return name;
        }
        private VerticaType getType() {
            return type;
        }
    }

    private static class TableSchema {
        private final List<Column> columns = Lists.newArrayList();
        private TableSchema(final List<String> columns, final Map<String, VerticaType> columnTypes) {
            for (final String column : columns) {
                final VerticaType type = columnTypes.get(column);
                Assert.isTrue(type != null, "No type found for column " + column);
                this.columns.add(new Column(column, type));
            }
        }
        private List<Column> getColumns() {
            return ImmutableList.copyOf(columns);
        }
    }

    private class TableInfo {
        private final String table;
        private final File file;
        private boolean populated;
        private final Optional<TableSchema> tableSchema;

        private TableInfo(final String table, final File file, final boolean populated,
                          final Optional<TableSchema> tableSchema) {
            this.table = table;
            this.file = file;
            this.populated = populated;
            this.tableSchema = tableSchema;
        }

        private String getTable() {
            return table;
        }

        private File getFile() {
            return file;
        }

        private List<Column> getColumns() {
            Assert.isTrue(tableSchema != null && tableSchema.isPresent(), "No columns supplied for table " + getTable());
            return ImmutableList.copyOf(tableSchema.get().getColumns());
        }

        private boolean isPopulated() {
            return populated;
        }

        private void setPopulated(final boolean populated) {
            this.populated = populated;
        }
    }

    private final VerticaConnection connection;
    private final Map<String, TableInfo> tables = Maps.newHashMap();
    private final boolean requireEmptyTables;
    private final boolean slowPopulate;

    public TablePopulator(final VerticaConnection connection, final List<File> files, final String schema,
                          final List<String> tables, final boolean requireEmptyTables, final boolean slowPopulate,
                          final Map<String, TableSchema> tableSchemas) throws IOException, SQLException {
        this.connection = connection;
        Assert.isTrue(files.size() == tables.size(), "While creating TablePopulator, found + " + files.size() + " files and " + tables.size() + "tables");
        for (int i = 0; i < tables.size(); i++) {
            final File file = files.get(i);
            final String fullName = schema + ".\"" + tables.get(i) + "\"";
            final Optional<TableSchema> tableSchema = tableSchemas.get(fullName) != null ? Optional.of(tableSchemas.get(fullName)) : Optional.<TableSchema>empty();
            this.tables.put(fullName, new TableInfo(fullName, file, false, tableSchema));
        }
        this.requireEmptyTables = requireEmptyTables;
        this.slowPopulate = slowPopulate;
    }

    public void populate() throws FileNotFoundException, SQLException {
        if (requireEmptyTables()) {
            clear();
        }
        for (final String table : getTables()) {
            if (slowPopulate()) {
                slowPopulateTable(table);
            } else {
                populateTable(table);
            }
        }
    }

    public Map<String, Integer> getRowCounts() throws SQLException {
        final Map<String, Integer> rowCounts = Maps.newHashMap();
        for (final String table : getTables()) {
            final int rowCount = getRowCount(table);
            rowCounts.put(table, rowCount);
        }
        return ImmutableMap.copyOf(rowCounts);
    }

    public int getRowCount(final String table) throws SQLException {
        final String query = "SELECT COUNT(*) AS c FROM " + table;
        final Statement stmt = connection.createStatement();
        final ResultSet rs = stmt.executeQuery(query);
        rs.next();
        final int count = rs.getInt("c");
        rs.close();
        stmt.close();
        return count;
    }

    private void populateTable(final String table) throws FileNotFoundException, SQLException {
        Assert.isTrue(!isPopulated(table), "Table population should only happen once");
        if (requireEmptyTables()) {
            checkTableIsEmpty(table);
        }
        final File file = getFile(table);
        System.err.println("Populating " + table + " from " + file.getAbsolutePath() + ": " + currentTime());
//        LOGGER.info("Populating " + table + " from " + file.getAbsolutePath());
        final String copyStmt = "COPY " + table + " FROM LOCAL STDIN DELIMITER E'\t'";
        final VerticaCopyStream copyStream = new VerticaCopyStream(getConnection(), copyStmt, new FileInputStream(file));
        copyStream.start();
        final List<Long> rejects = copyStream.getRejects();
        Collections.sort(rejects);
        final int numRejects = rejects.size();
        final long numResults = copyStream.finish();
        System.err.println("Number of rows inserted: " + numResults);
        System.err.println("Number of rows rejected: " + numRejects);
        if (numRejects == 0) {
            setPopulated(table, true);
        } else {
            printRejectedRows(rejects);
            if (numRejects == 1 && rejects.get(0) == 1) {
                System.err.println("Rejected first line is assumed to be a header - ignoring");
                setPopulated(table, true);
            } else {
                printAcceptedRows(rejects, numResults);
                System.err.println("Rolling back: " + currentTime());
                clearTable(table);
            }
        }
    }

    private void slowPopulateTable(final String table) throws SQLException, FileNotFoundException {
        Assert.isTrue(!isPopulated(table), "Table population should only happen once");
        if (requireEmptyTables()) {
            checkTableIsEmpty(table);
        }
        final File file = getFile(table);
        System.err.println("Populating " + table + " from " + file.getAbsolutePath() + ": " + currentTime());
//        LOGGER.info("Populating " + table + " from " + file.getAbsolutePath());
        final Scanner sc = new Scanner(file);
        sc.nextLine(); // header
        final List<Column> columns = getTableInfo(table).getColumns();
        final int numCols = columns.size();
        final String insertSql = "INSERT INTO " + table + " VALUES (" + StringUtils.repeat("?, ", numCols - 1) + "?)";
        final PreparedStatement stmt = getConnection().prepareStatement(insertSql);
        int rowCount = 1;
        connection.setAutoCommit(false);
        final List<Integer> allCounts = Lists.newArrayList();
        final int BATCH_SIZE = 1000000;
        while (sc.hasNextLine()) {
            final boolean endOfBatch = rowCount % BATCH_SIZE == 0;
            if (endOfBatch) {
                System.err.println("On row number " + rowCount + ": " + currentTime());
            }
            final List<String> values = Lists.newArrayList(sc.nextLine().split("\t"));
//            if (printDebugging) {
//                System.err.println("Checking values: " + currentTime());
//            }
            Assert.isTrue(values.size() <= numCols, "More values (" + values.size() + ") than columns (" + numCols + ")");
            while (values.size() < numCols) {
                values.add(null);
            }
//            if (printDebugging) {
//                System.err.println("Setting values: " + currentTime());
//            }
            setPreparedStatementValues(stmt, columns, values);
//            if (printDebugging) {
//                System.err.println("Executing update: " + currentTime());
//            }
//            stmt.executeUpdate();
            stmt.addBatch();
//            if (printDebugging) {
//                System.err.println("Added to batch: " + currentTime());
//            }
            if (endOfBatch) {
                addNextBatchCounts(stmt, BATCH_SIZE, table, allCounts, true);
            }
            rowCount++;
        }
        System.err.println("Executing final batch: " + currentTime());
        addNextBatchCounts(stmt, BATCH_SIZE, table, allCounts, false);
        connection.commit();
        stmt.close();
        sc.close();
        connection.setAutoCommit(true);

        Assert.isTrue(allCounts.size() == (rowCount-1), allCounts.size() + " != " + (rowCount-1));
        printAllRows(allCounts, rowCount, table, true);

    }

    private void addNextBatchCounts(final PreparedStatement stmt, final int batchSize, final String table, final List<Integer> allCounts, final boolean checkCount) throws SQLException {
        int[] counts = tryExecuteBatch(stmt, batchSize, table);
        Assert.isTrue(!checkCount || counts.length == batchSize, counts.length + " != " + batchSize);
        for (final int count : counts) {
            allCounts.add(count);
        }
    }

    private int[] tryExecuteBatch(final PreparedStatement stmt, final int batchSize, final String table) throws SQLException {
        int[] counts = null;
        try {
            counts = stmt.executeBatch();
        } catch (final BatchUpdateException e) {
            counts = e.getUpdateCounts();
            final List<Integer> countsList = Lists.newArrayList();
            for (final int count : counts) {
                countsList.add(count);
            }
            printAllRows(countsList, batchSize, table, false);
            e.printStackTrace();
            System.exit(1);
        }
        return counts;
    }

    private void printAllRows(final List<Integer> counts, final int totalCount, final String table, final boolean rollback) throws SQLException {
        final List<Long> rejects = Lists.newArrayList();
        for (int i = 0; i < counts.size(); i++) {
            final int count = counts.get(i);
            if (count == Statement.EXECUTE_FAILED) {
                rejects.add((long) (i+1));
            }
        }
        final int numRejects = rejects.size();
        if (numRejects > 0) {
            printRejectedRows(rejects);
            if (numRejects == 1 && rejects.get(0) == 1) {
                System.err.println("Rejected first line is assumed to be a header - ignoring");
                setPopulated(table, true);
            } else {
                printAcceptedRows(rejects, totalCount);
                if (rollback) {
                    System.err.println("Rolling back: " + currentTime());
                    clearTable(table);
                }
            }
        } else {
            setPopulated(table, true);
        }
    }

    private void setPreparedStatementValues(final PreparedStatement stmt, final List<Column> columns, final List<String> values) throws SQLException {
        for (int i = 0; i < columns.size(); i++) {
            final VerticaType colType = columns.get(i).getType();
            setPreparedStatementValue(stmt, i+1, values.get(i), colType);
        }
    }

    private void setPreparedStatementValue(final PreparedStatement stmt, final int index, String value, final VerticaType colType) throws SQLException {
        if (value != null && value.equalsIgnoreCase("NULL")) {
            value = null;
        }
        switch (colType) {
            case VARCHAR:
                if (value == null) {
                    stmt.setNull(index, Types.VARCHAR);
                } else {
                    stmt.setString(index, value);
                }
                return;
            case INTEGER:
                if (value == null) {
                    stmt.setNull(index, Types.INTEGER);
                } else {
                    stmt.setInt(index, Integer.parseInt(value));
                }
                return;
            case NUMERIC:
                if (value == null) {
                    stmt.setNull(index, Types.NUMERIC);
                } else {
                    stmt.setDouble(index, Double.parseDouble(value));
                }
                return;
            case BOOLEAN:
                if (value == null) {
                    stmt.setNull(index, Types.BOOLEAN);
                } else {
                    stmt.setBoolean(index, Boolean.parseBoolean(value));
                }
                return;
            default:
                throw new IllegalArgumentException("Unrecognized column type " + colType.toString());
        }
    }

    private void printRejectedRows(final List<Long> rejects) {
        final int numRejects = rejects.size();
        Assert.isTrue(numRejects >= 1, "Trying to print rejected rows but there aren't any");
        final int maxNumRowsToPrint = 40;
        String toPrint = "Rejected row numbers: ";
        if (numRejects < maxNumRowsToPrint) {
            toPrint += StringUtils.join(rejects, ",");
        } else {
            toPrint += StringUtils.join(rejects.subList(0, maxNumRowsToPrint / 2), ",") + ",...,"
                    + StringUtils.join(rejects.subList(numRejects - maxNumRowsToPrint / 2, numRejects), ",");
        }
        System.err.println(toPrint);
    }

    private void printAcceptedRows(final List<Long> rejects, final long numResults) {
        final int maxNumRowsToPrint = 40;
        final long numTotalRows = rejects.size() + numResults;
        final List<Long> accepted = new ArrayList<Long>();
        int acceptedRowCount = 0;
        for (long i = 1; i <= numTotalRows; i++) {
            if (acceptedRowCount >= maxNumRowsToPrint / 2) {
                break;
            }
            if (!sortedContains(rejects, i)) {
                accepted.add(i);
                acceptedRowCount++;
            }
        }
        for (long i = numTotalRows; i > 0; i--) {
            if (acceptedRowCount >= maxNumRowsToPrint) {
                break;
            }
            if (!sortedContains(rejects, i)) {
                accepted.add(i);
                acceptedRowCount++;
            }
        }
        Collections.sort(accepted);
        String toPrint = "Accepted rows: ";
        final int numAccepted = accepted.size();
//        if (numAccepted != numResults) {
//            throw new AssertionError("Number of accepted rows calculated as " + numAccepted + " but returned as " + numResults);
//        }
        if (numAccepted < maxNumRowsToPrint) {
            toPrint += StringUtils.join(accepted, ",");
        } else {
            toPrint += StringUtils.join(accepted.subList(0, maxNumRowsToPrint / 2), ",") + ",...,"
                    + StringUtils.join(accepted.subList(numAccepted - maxNumRowsToPrint / 2, numAccepted), ",");
        }
        System.err.println(toPrint);
    }

    private static boolean sortedContains(final List<Long> sortedList, final long target) {
        long prevValue = Long.MIN_VALUE;
        for (final long value : sortedList) {
            Assert.isTrue(value >= prevValue, "List is not sorted");
            final long comparison = value - target;
            if (comparison == 0) {
                return true;
            } else if (comparison > 0) {
                return false;
            }
            prevValue = value;
        }
        return false;
    }

    public void clear() throws SQLException {
        for (final String table : getTables()) {
            clearTable(table);
        }
    }

    public void clearTable(final String table) throws SQLException {
        final VerticaConnection conn = getConnection();
        final String update = "DELETE FROM " + table;
        final Statement stmt = conn.createStatement();
        stmt.executeUpdate(update);
        stmt.close();
        setPopulated(table, false);
    }

    private void checkTableIsEmpty(final String table) throws SQLException {
        final String query = "SELECT COUNT(*) AS c FROM " + table;
        final Statement stmt = getConnection().createStatement();
        final ResultSet rs = stmt.executeQuery(query);
        Assert.isTrue(rs.next(), "No row count returned from " + table);
        final int rowCount = rs.getInt("c");
        rs.close();
        stmt.close();
        Assert.isTrue(rowCount == 0, "Table is not empty even though clearing should have been performed");
    }

    private void createTablesFromFile(final File schemaFile) throws IOException, SQLException {
        final Scanner sc = new Scanner(schemaFile);
        sc.useDelimiter("");
        String sql = "";
        while (sc.hasNext()) {
            final String s = sc.next();
            if (s.equals(";")) {
                createOrDropTable(sql);
                sql = "";
            } else {
                sql += s;
            }
        }
        sc.close();
    }

    private void createOrDropTable(final String sql) throws SQLException {
        final Statement stmt = getConnection().createStatement();
        stmt.executeUpdate(sql);
        stmt.close();
    }

    public boolean slowPopulate() {
        return slowPopulate;
    }

    public boolean requireEmptyTables() {
        return requireEmptyTables;
    }

    public Map<String, TableInfo> getTablesMap() {
        return ImmutableMap.copyOf(tables);
    }

    public List<String> getTables() {
        final List<String> tables = new ArrayList<String>(getTablesMap().keySet());
        Collections.sort(tables);
        return ImmutableList.copyOf(tables);
    }

    public VerticaConnection getConnection() {
        return connection;
    }

    public List<File> getFiles() {
        final List<File> files = Lists.newArrayList();
        for (final TableInfo info : getTablesMap().values()) {
            files.add(info.getFile());
        }
        Collections.sort(files);
        return ImmutableList.copyOf(files);
    }

    public File getFile(final String table) {
        return getTableInfo(table).getFile();
    }

    public boolean isPopulated(final String table) {
        return getTableInfo(table).isPopulated();
    }

    public void setPopulated(final String table, final boolean populated) {
        getTableInfo(table).setPopulated(populated);
    }

    public int getNumTables() {
        return getTablesMap().size();
    }

    private void clean() throws SQLException, IOException {
        final Connection conn = getConnection();
        if (!conn.isClosed()) {
            conn.close();
        }
    }

    private TableInfo getTableInfo(final String table) {
        return getTablesMap().get(table);
    }

    private static Options getOptions() {
        final Options options = new Options();

        options.addOption("f", "flat_files", true, "One or more flat files (comma-delimited) with data to insert");
        options.getOption("f").setRequired(true);

        options.addOption("s", "schema_name", true, "Name of database schema to use");
        options.getOption("s").setRequired(false);

        options.addOption("t", "tables", true, "One or more tables (comma-delimited) to populate");
        options.getOption("t").setRequired(true);

        options.addOption("url", "vertica_url", true, "URL of Vertica database to use");
        options.getOption("url").setRequired(false);

        options.addOption("u", "user", true, "Username for Vertica database");
        options.getOption("u").setRequired(true);

        options.addOption("p", "password", true, "Password for Vertica database");
        options.getOption("p").setRequired(true);

        options.addOption("nc", "no_clear", false, "Do not clear table before populating it");
        options.getOption("nc").setRequired(false);

        options.addOption("cf", "create_file", true, "File to use to create/drop one or more tables before populating table");
        options.getOption("cf").setRequired(false);

        options.addOption("ku", "keep_unzipped", false, "Keep unzipped file if supplying a GZIP compresssed file");
        options.getOption("ku").setRequired(false);

        options.addOption("ud", "unzipped_dir", true, "Directory for storage of any files unzipped on the fly");
        options.getOption("ud").setRequired(false);

        options.addOption("slow", false, "Populate table using INSERT (when COPY command doesn't work)");
        options.getOption("slow").setRequired(false);

        options.addOption("c", "columns", true, "List of columns per table (columns comma-delimited, tables semicolon-delimited) (only required for slow population if create table SQL file is not supplied)");
        options.getOption("c").setRequired(false);

        options.addOption("ct", "column_types", true, "List of column types (VARCHAR|INTEGER|NUMERIC|BOOLEAN) corresponding to tables in -c argument with same delimiting (only required for slow population if create table SQL file is not supplied)");
        options.getOption("ct").setRequired(false);

        return options;
    }

    public static void main(final String[] args) throws ParseException, SQLException, IOException {
        System.err.println("Begin: " + currentTime());

        final Options options = getOptions();
        final CommandLineParser parser = new DefaultParser();
        final CommandLine cmd = parser.parse(options, args);

        final String[] filepaths = cmd.getOptionValue("f").split(",");
        final String[] tables = cmd.getOptionValue("t").split(",");
        if (filepaths.length != tables.length) {
            throw new IllegalArgumentException("Different numbers of files (" + filepaths.length
                    + ") and tables (" + tables.length + ") provided");
        }
        final String schema = cmd.getOptionValue("s", "omicsoft");
        final String url = cmd.getOptionValue("url", "jdbc:vertica://cam-prod-vertica12.nibr.novartis.net:5433/v_nibr_prod");
        final String user = cmd.getOptionValue("u");
        final String pw = cmd.getOptionValue("p");
        final Optional<String> schemaFile = cmd.hasOption("cf") ? Optional.of(cmd.getOptionValue("cf")) : Optional.<String>empty();
        final Optional<List<String>> columnsOpt = cmd.hasOption("c") ? Optional.of(Arrays.asList(cmd.getOptionValue("c").split(";"))) : Optional.<List<String>>empty();
        final Optional<List<String>> columnTypesOpt = cmd.hasOption("ct") ? Optional.of(Arrays.asList(cmd.getOptionValue("ct").split(";"))) : Optional.<List<String>>empty();
        final boolean noClear = cmd.hasOption("nc");
        final boolean keepUnzipped = cmd.hasOption("ku");
        final String unzippedDir = cmd.getOptionValue("ud", null);
        final boolean slowPopulate = cmd.hasOption("slow");
        if (slowPopulate && !schemaFile.isPresent() && (!columnsOpt.isPresent() || !columnTypesOpt.isPresent())) {
            throw new IllegalArgumentException("Table schema SQL file or lists of columns and types are required for slow population");
        }
        final Map<String, TableSchema> tableToSchema = Maps.newHashMap();
        if (columnsOpt.isPresent() && columnTypesOpt.isPresent()) {
            final List<String> tableColumnStrings = columnsOpt.get();
            final List<String> tableColumnTypeStrings = columnTypesOpt.get();
            if (tableColumnStrings.size() != tableColumnTypeStrings.size() || tableColumnStrings.size() != tables.length) {
                throw new IllegalArgumentException("List of columns, types, and tables are not the same length");
            }
            for (int i = 0; i < tableColumnStrings.size(); i++) {
                final String table = tables[i];
                final String[] tableColumns = tableColumnStrings.get(i).split(",");
                final String[] tableColumnTypes = tableColumnTypeStrings.get(i).split(",");
                final TableSchema tableSchema = createTableSchemaFromColumnsAndTypes(tableColumns, tableColumnTypes);
                tableToSchema.put(table, tableSchema);
            }
        } else if (schemaFile.isPresent()) {
            tableToSchema.putAll(createTableSchemaMapFromFile(schemaFile.get(), schema));
        }

        final Properties connectionProps = new Properties();
        connectionProps.put("user", user);
        connectionProps.put("password", pw);
        final Connection conn = DriverManager.getConnection(url, connectionProps);

        final List<File> toDelete = Lists.newArrayList();
        final List<File> files = Lists.newArrayList();
        for (int i = 0; i < filepaths.length; i++) {
            final String filepath = filepaths[i];
            final String table = tables[i];
            final File file;
            if (filepath.endsWith(".gz")) {
                System.err.println("Unzipping " + filepath + ": " + currentTime());
                file = gunzip(filepath, unzippedDir != null ? Optional.of(unzippedDir + "/" + table + ".txt") : Optional.<String>empty());
                if (!keepUnzipped) {
                    toDelete.add(file);
                }
            } else {
                file = new File(filepath);
            }
            files.add(file);
        }

        final TablePopulator tablePopulator = new TablePopulator((VerticaConnection) conn, files, schema, Arrays.asList(tables), !noClear, slowPopulate, ImmutableMap.copyOf(tableToSchema));
        if (schemaFile.isPresent()) {
            System.err.println("Executing create table SQL from " + schemaFile.get() + ": " + currentTime());
            tablePopulator.createTablesFromFile(new File(schemaFile.get()));
        }
        System.err.println("Populating tables: " + currentTime());
        tablePopulator.populate();
        tablePopulator.clean();
        for (final File file : toDelete) {
            System.err.println("Deleting file " + file.getAbsolutePath() + ": " + currentTime());
            Files.delete(file.toPath());
        }
        System.err.println("End: " + currentTime());
    }

    private static TableSchema createTableSchemaFromColumnsAndTypes(final String[] columns, final String[] columnTypes) {
        Assert.isTrue(columns.length == columnTypes.length, "Lists of columns and column types are not the same size");
        final Map<String, VerticaType> columnToType = Maps.newHashMap();
        for (int i = 0; i < columns.length; i++) {
            columnToType.put(columns[i], VerticaType.valueOf(columnTypes[i]));
        }
        return new TableSchema(Arrays.asList(columns), columnToType);
    }

    private static Map<String, TableSchema> createTableSchemaMapFromFile(final String file, final String schema) throws FileNotFoundException {
        final Map<String, TableSchema> tableToSchema = Maps.newHashMap();
        final Pattern createLinePattern = Pattern.compile("CREATE TABLE (?:IF NOT EXISTS )?(" + schema + ".\".+\") \\(");
        final Pattern columnLinePattern = Pattern.compile("\\s*\"(.+)\" (.+)( NOT NULL)?,?");
        final String endTableLine = ");";
        List<String> columns = Lists.newArrayList();
        Map<String, VerticaType> columnToType = Maps.newHashMap();
        final Scanner sc = new Scanner(new File(file));
        while (sc.hasNextLine()) {
            final String line = sc.nextLine();
            final Matcher createLineMatcher = createLinePattern.matcher(line);
            if (createLineMatcher.matches()) {
                final String table = createLineMatcher.group(1);
                while (sc.hasNextLine()) {
                    final String columnLine = sc.nextLine();
                    if (columnLine.equals(endTableLine)) {
                        tableToSchema.put(table, new TableSchema(columns, columnToType));
                        columns = Lists.newArrayList();
                        columnToType = Maps.newHashMap();
                        break;
                    }
                    final Matcher columnLineMatcher = columnLinePattern.matcher(columnLine);
                    if (columnLineMatcher.matches()) {
                        final String columnName = columnLineMatcher.group(1);
                        final VerticaType columnType = columnTypeStrToType((columnLineMatcher.group(2)));
                        columns.add(columnName);
                        columnToType.put(columnName, columnType);
                    }
                }
            }
        }
        sc.close();
        return tableToSchema;
    }

    private static VerticaType columnTypeStrToType(final String typeStr) {
        for (final VerticaType type : VerticaType.values()) {
            if (typeStr.toUpperCase().startsWith(type.toString())) { // get rid of possible length suffix
                return type;
            }
        }
        if (typeStr.toUpperCase().startsWith("NUMBER")) {
            return VerticaType.NUMERIC;
        }
        throw new IllegalArgumentException("No Vertica type found for alleged type " + typeStr);
    }

    private static String currentTime() {
        return DATE_TIME_FORMAT.format(new Date());
    }
    private static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private static File gunzip(final String filepath, final Optional<String> newFilepath) throws IOException {
        Assert.isTrue(filepath.endsWith(".gz"), "Trying to gunzip file that doesn't end with .gz: " + filepath);
        final String tempFile = newFilepath.orElse("resources/" + FilenameUtils.getBaseName(filepath));
        final GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(filepath));
        final FileOutputStream out = new FileOutputStream(tempFile);
        int len;
        final byte[] buffer = new byte[1024];
        while ((len = gzis.read(buffer)) > 0) {
            out.write(buffer, 0, len);
        }
        gzis.close();
        out.close();
        return new File(tempFile);
    }

}
