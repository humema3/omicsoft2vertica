DROP TABLE IF EXISTS omicsoft."TCGA_ExonAnnotation";
CREATE TABLE omicsoft."TCGA_ExonAnnotation" (
	"ExonIndex" INTEGER NOT NULL,
	"ExonID" VARCHAR(22) NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"GeneID" VARCHAR(40) NOT NULL,
	"GeneName" VARCHAR(40) NOT NULL,
	"Transcripts" VARCHAR(1308) NOT NULL,
	"IsUnique" BOOLEAN NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_AgilentExpressionRatio";
CREATE TABLE omicsoft."TCGA_AgilentExpressionRatio" (
	"SampleIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Value" NUMBER(9,4) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_CNVCalls";
CREATE TABLE omicsoft."TCGA_CNVCalls" (
	"SampleIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Call" VARCHAR(21) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_CNVGeneLevelData";
CREATE TABLE omicsoft."TCGA_CNVGeneLevelData" (
	"SampleIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Value" NUMBER(9,4) NOT NULL,
	"MinSegmentLength" INTEGER NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_CNVSegmentGeneMapping";
CREATE TABLE omicsoft."TCGA_CNVSegmentGeneMapping" (
	"SegmentIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_CNVSegments";
CREATE TABLE omicsoft."TCGA_CNVSegments" (
	"SampleIndex" INTEGER NOT NULL,
	"SegmentIndex" INTEGER NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"Log2Ratio" NUMBER(9,4) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_DnaSeqSomaticMutation";
CREATE TABLE omicsoft."TCGA_DnaSeqSomaticMutation" (
	"SampleIndex" INTEGER NOT NULL,
	"MutationIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"Mutation" VARCHAR(3711) NOT NULL,
	"Coverage" VARCHAR(5) NOT NULL,
	"Frequency" NUMBER(7,3)
);


DROP TABLE IF EXISTS omicsoft."TCGA_ExonJunctionAnnotation";
CREATE TABLE omicsoft."TCGA_ExonJunctionAnnotation" (
	"ExonJunctionIndex" INTEGER NOT NULL,
	"ExonJunctionID" VARCHAR(22) NOT NULL,
	"Strand" VARCHAR(1) NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"IntronPosition" INTEGER NOT NULL,
	"IntronSize" INTEGER NOT NULL,
	"Type" VARCHAR(5) NOT NULL,
	"KnownGene" VARCHAR(443) NOT NULL,
	"KnownTranscript" VARCHAR(2441) NOT NULL,
	"KnownExonNumber" VARCHAR(625) NOT NULL,
	"InferredName" VARCHAR(49) NOT NULL,
	"FrameShift" NUMBER(5,1),
	"FrameShiftType" NUMBER(5,1)
);


DROP TABLE IF EXISTS omicsoft."TCGA_ExonJunctionData";
CREATE TABLE omicsoft."TCGA_ExonJunctionData" (
	"SampleIndex" INTEGER NOT NULL,
	"ExonJunctionIndex" INTEGER NOT NULL,
	"ReadCount" INTEGER NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_FusionAnnotation";
CREATE TABLE omicsoft."TCGA_FusionAnnotation" (
	"FusionIndex" INTEGER NOT NULL,
	"FusionID" VARCHAR(29) NOT NULL,
	"Strand" VARCHAR(2) NOT NULL,
	"Chromosome1" VARCHAR(2) NOT NULL,
	"Position1" INTEGER NOT NULL,
	"Chromosome2" VARCHAR(2) NOT NULL,
	"Position2" INTEGER NOT NULL,
	"KnownGene1" VARCHAR(358) NOT NULL,
	"KnownTranscript1" VARCHAR(1308) NOT NULL,
	"KnownExonNumber1" VARCHAR(351) NOT NULL,
	"KnownTranscriptStrand1" VARCHAR(119) NOT NULL,
	"KnownGene2" VARCHAR(358) NOT NULL,
	"KnownTranscript2" VARCHAR(1308) NOT NULL,
	"KnownExonNumber2" VARCHAR(351) NOT NULL,
	"KnownTranscriptStrand2" VARCHAR(119) NOT NULL,
	"FusionGene" VARCHAR(527) NOT NULL,
	"SplicePattern" VARCHAR(5) NOT NULL,
	"SplicePatternClass" VARCHAR(23) NOT NULL,
	"FrameShift" VARCHAR(8) NOT NULL,
	"FrameShiftClass" VARCHAR(10) NOT NULL,
	"Distance" INTEGER NOT NULL,
	"OnExonBoundary" VARCHAR(6) NOT NULL,
	"Filter" VARCHAR(15)
);


DROP TABLE IF EXISTS omicsoft."TCGA_FusionData";
CREATE TABLE omicsoft."TCGA_FusionData" (
	"SampleIndex" INTEGER NOT NULL,
	"FusionIndex" INTEGER NOT NULL,
	"GeneID1" VARCHAR(40) NOT NULL,
	"GeneID2" VARCHAR(40) NOT NULL,
	"UniqueCuttingPosition" INTEGER NOT NULL,
	"SeedCount" INTEGER NOT NULL,
	"RescuedCount" INTEGER NOT NULL,
	"UniqueRead1Position" INTEGER NOT NULL,
	"UniqueRead2Position" INTEGER NOT NULL,
	"PairedEndCount" INTEGER NOT NULL,
	"SingleEnd.RPKM" NUMBER(12,4) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_GeneAnnotation";
CREATE TABLE omicsoft."TCGA_GeneAnnotation" (
	"GeneIndex" INTEGER NOT NULL,
	"GeneID" VARCHAR(40) NOT NULL,
	"EntrezID" NUMBER(13,1),
	"GeneName" VARCHAR(40) NOT NULL,
	"TranscriptNumber" INTEGER NOT NULL,
	"Strand" VARCHAR(1) NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"ExonLength" INTEGER NOT NULL,
	"Source" VARCHAR(16) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_GeneFPKM";
CREATE TABLE omicsoft."TCGA_GeneFPKM" (
	"SampleIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"FPKM" NUMBER(14,4) NOT NULL,
	"Count" NUMBER(15,4) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_Methylation450";
CREATE TABLE omicsoft."TCGA_Methylation450" (
	"SampleIndex" INTEGER NOT NULL,
	"ProbeIndex" INTEGER NOT NULL,
	"Value" NUMBER(8,4) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_MethylationProbes";
CREATE TABLE omicsoft."TCGA_MethylationProbes" (
	"ProbeIndex" INTEGER NOT NULL,
	"ProbeID" VARCHAR(16) NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Type" VARCHAR(2213)
);


DROP TABLE IF EXISTS omicsoft."TCGA_miRNAAnnotation";
CREATE TABLE omicsoft."TCGA_miRNAAnnotation" (
	"MirnaIndex" INTEGER NOT NULL,
	"MirnaID" VARCHAR(11) NOT NULL,
	"EntrezID" NUMBER(13,1),
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"Strand" VARCHAR(1) NOT NULL,
	"Name" VARCHAR(15) NOT NULL,
	"Alias" VARCHAR(9) NOT NULL,
	"MatureMirna_Name" VARCHAR(62),
	"MatureMirna_Alias" VARCHAR(54),
	"MatureMirna_Coordinates" VARCHAR(98)
);


DROP TABLE IF EXISTS omicsoft."TCGA_miRnaSeqQCTable";
CREATE TABLE omicsoft."TCGA_miRnaSeqQCTable" (
	"SampleIndex" INTEGER NOT NULL,
	"AttributeName" VARCHAR(41) NOT NULL,
	"AttributeType" VARCHAR(7) NOT NULL,
	"AttributeValue" VARCHAR(9) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_miRNASeq";
CREATE TABLE omicsoft."TCGA_miRNASeq" (
	"SampleIndex" INTEGER NOT NULL,
	"MirnaIndex" INTEGER NOT NULL,
	"TranscriptName" VARCHAR(19) NOT NULL,
	"NormalizedCount" NUMBER(13,4) NOT NULL,
	"Count" NUMBER(12,1) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_MutationAnnotation";
CREATE TABLE omicsoft."TCGA_MutationAnnotation" (
	"MutationIndex" INTEGER NOT NULL,
	"AttributeName" VARCHAR(31) NOT NULL,
	"AttributeType" VARCHAR(7) NOT NULL,
	"AttributeValue" VARCHAR(79) NOT NULL
);

DROP TABLE IF EXISTS omicsoft."TCGA_Mutations_Canonical";
CREATE TABLE omicsoft."TCGA_Mutations_Canonical" (
	"MutationIndex" INTEGER NOT NULL,
	"MutationID" VARCHAR(3722) NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Position" INTEGER NOT NULL,
	"ReferenceAllele" VARCHAR(1) NOT NULL,
	"MutationAllele" VARCHAR(3711) NOT NULL,
	"GeneID" VARCHAR(355) NOT NULL,
	"GeneName" VARCHAR(190) NOT NULL,
	"dbSNPID" VARCHAR(444),
	"dbSNPCategory" VARCHAR(189),
	"AnnotationType" VARCHAR(258) NOT NULL,
	"AAPosition" VARCHAR(103) NOT NULL,
	"AAChange" VARCHAR(46),
	"AAMutation" VARCHAR(187),
	"AAFrame" VARCHAR(44),
	"TranscriptID" VARCHAR(355),
	"TranscriptName" VARCHAR(274),
	"TranscriptStrand" VARCHAR(34),
	"DistanceTo5Prime" VARCHAR(159) NOT NULL,
	"DistanceTo3Prime" VARCHAR(145) NOT NULL,
	"DistanceToExonBoundary" VARCHAR(130) NOT NULL,
	"Uniprot_acc" VARCHAR(4072),
	"Uniprot_id" VARCHAR(6130),
	"Ensembl_geneid" VARCHAR(9280),
	"SIFT_score" NUMBER(7,3),
	"SIFT_pred" VARCHAR(13),
	"Polyphen2_HDIV_score" NUMBER(7,3),
	"Polyphen2_HDIV_pred" VARCHAR(13),
	"Polyphen2_HVAR_score" NUMBER(7,3),
	"Polyphen2_HVAR_pred" VARCHAR(13),
	"Cosmic.ID" VARCHAR(359),
	"Cosmic.Gene" VARCHAR(441),
	"Cosmic.Strand" VARCHAR(59),
	"Cosmic.CDS" VARCHAR(299),
	"Cosmic.AA" VARCHAR(239),
	"Cosmic.CNT" VARCHAR(59),
	"1000Genome.AlleleFrequency" NUMBER(7,3),
	"1000Genome.AFR.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.AMR.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.EAS.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.EUR.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.SAS.AlleleFrequency.v5" NUMBER(7,3),
	"ucscGenePfam" VARCHAR(223)
);


DROP TABLE IF EXISTS omicsoft."TCGA_Mutations";
CREATE TABLE omicsoft."TCGA_Mutations" (
	"MutationIndex" INTEGER NOT NULL,
	"MutationID" VARCHAR(3722) NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Position" INTEGER NOT NULL,
	"ReferenceAllele" VARCHAR(1) NOT NULL,
	"MutationAllele" VARCHAR(3711) NOT NULL,
	"GeneID" VARCHAR(448) NOT NULL,
	"GeneName" VARCHAR(448) NOT NULL,
	"dbSNPID" VARCHAR(444),
	"dbSNPCategory" VARCHAR(189),
	"AnnotationType" VARCHAR(2339) NOT NULL,
	"AAPosition" VARCHAR(612) NOT NULL,
	"AAChange" VARCHAR(1849),
	"AAMutation" VARCHAR(1955),
	"AAFrame" VARCHAR(686),
	"TranscriptID" VARCHAR(1474) NOT NULL,
	"TranscriptName" VARCHAR(1474) NOT NULL,
	"TranscriptStrand" VARCHAR(211) NOT NULL,
	"DistanceTo5Prime" VARCHAR(979) NOT NULL,
	"DistanceTo3Prime" VARCHAR(958) NOT NULL,
	"DistanceToExonBoundary" VARCHAR(731) NOT NULL,
	"Uniprot_acc" VARCHAR(25252),
	"Uniprot_id" VARCHAR(7006),
	"Ensembl_geneid" VARCHAR(10606),
	"SIFT_score" NUMBER(7,3),
	"SIFT_pred" VARCHAR(13),
	"Polyphen2_HDIV_score" NUMBER(7,3),
	"Polyphen2_HDIV_pred" VARCHAR(13),
	"Polyphen2_HVAR_score" NUMBER(7,3),
	"Polyphen2_HVAR_pred" VARCHAR(13),
	"Cosmic.ID" VARCHAR(359),
	"Cosmic.Gene" VARCHAR(441),
	"Cosmic.Strand" VARCHAR(59),
	"Cosmic.CDS" VARCHAR(299),
	"Cosmic.AA" VARCHAR(239),
	"Cosmic.CNT" VARCHAR(59),
	"1000Genome.AlleleFrequency" NUMBER(7,3),
	"1000Genome.AFR.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.AMR.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.EAS.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.EUR.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.SAS.AlleleFrequency.v5" NUMBER(7,3),
	"ucscGenePfam" VARCHAR(223)
);


DROP TABLE IF EXISTS omicsoft."TCGA_RnaSeqMutation";
CREATE TABLE omicsoft."TCGA_RnaSeqMutation" (
	"SampleIndex" INTEGER NOT NULL,
	"MutationIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"Mutation" VARCHAR(12) NOT NULL,
	"Coverage" INTEGER NOT NULL,
	"Frequency" NUMBER(7,3) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_RnaSeqQCTable";
CREATE TABLE omicsoft."TCGA_RnaSeqQCTable" (
	"SampleIndex" INTEGER NOT NULL,
	"AttributeName" VARCHAR(57) NOT NULL,
	"AttributeType" VARCHAR(7) NOT NULL,
	"AttributeValue" VARCHAR(89) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_RnaSeqSomaticMutation";
CREATE TABLE omicsoft."TCGA_RnaSeqSomaticMutation" (
	"SampleIndex" INTEGER NOT NULL,
	"MutationIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"Mutation" VARCHAR(12) NOT NULL,
	"Coverage" INTEGER NOT NULL,
	"Frequency" NUMBER(7,3) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_RPPA_RBN";
CREATE TABLE omicsoft."TCGA_RPPA_RBN" (
	"SampleIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Transcript" VARCHAR(23) NOT NULL,
	"Value" NUMBER(9,4) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_RPPA";
CREATE TABLE omicsoft."TCGA_RPPA" (
	"SampleIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Transcript" VARCHAR(25) NOT NULL,
	"Value" NUMBER(9,4) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_SampleMetaData";
CREATE TABLE omicsoft."TCGA_SampleMetaData" (
	"SampleIndex" INTEGER NOT NULL,
	"AttributeName" VARCHAR(97) NOT NULL,
	"AttributeType" VARCHAR(7) NOT NULL,
	"IsCommon" VARCHAR(1) NOT NULL,
	"AttributeValue" VARCHAR(331) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_Samples";
CREATE TABLE omicsoft."TCGA_Samples" (
	"SampleIndex" INTEGER NOT NULL,
	"SampleID" VARCHAR(16) NOT NULL,
	"SubjectID" VARCHAR(12) NOT NULL,
	"TumorType" VARCHAR(4) NOT NULL,
	"SampleType" VARCHAR(47) NOT NULL,
	"Disease" VARCHAR(64) NOT NULL,
	"BamFileName" VARCHAR(40)
);


DROP TABLE IF EXISTS omicsoft."TCGA_TranscriptAnnotation";
CREATE TABLE omicsoft."TCGA_TranscriptAnnotation" (
	"TranscriptIndex" INTEGER NOT NULL,
	"TranscriptID" VARCHAR(15) NOT NULL,
	"TranscriptName" VARCHAR(17) NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"GeneID" VARCHAR(40) NOT NULL,
	"GeneName" VARCHAR(40) NOT NULL,
	"ExonNumber" INTEGER NOT NULL,
	"Strand" VARCHAR(1) NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"ExonLength" INTEGER NOT NULL,
	"Source" VARCHAR(16) NOT NULL,
	"RefSeqID" VARCHAR(12)
);


DROP TABLE IF EXISTS omicsoft."TCGA_TranscriptFPKM";
CREATE TABLE omicsoft."TCGA_TranscriptFPKM" (
	"SampleIndex" INTEGER NOT NULL,
	"TranscriptIndex" INTEGER NOT NULL,
	"FPKM" NUMBER(14,4) NOT NULL,
	"Count" NUMBER(15,4) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."TCGA_ExonData";
CREATE TABLE omicsoft."TCGA_ExonData" (
	"SampleIndex" INTEGER NOT NULL,
	"ExonIndex" INTEGER NOT NULL,
	"ReadCount" NUMBER(18,8) NOT NULL,
	"Rpkm" NUMBER(19,10) NOT NULL
);