DROP TABLE IF EXISTS omicsoft."CCLE_CNVCalls";
CREATE TABLE omicsoft."CCLE_CNVCalls" (
	"SampleIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Call" VARCHAR(21) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_CNVGeneLevelData";
CREATE TABLE omicsoft."CCLE_CNVGeneLevelData" (
	"SampleIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Value" NUMBER(8,4) NOT NULL,
	"MinSegmentLength" INTEGER NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_CNVSegmentGeneMapping";
CREATE TABLE omicsoft."CCLE_CNVSegmentGeneMapping" (
	"SegmentIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_CNVSegments";
CREATE TABLE omicsoft."CCLE_CNVSegments" (
	"SampleIndex" INTEGER NOT NULL,
	"SegmentIndex" INTEGER NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"Log2Ratio" NUMBER(8,4) NOT NULL,
	"HomozygosityRate" NUMBER(5,1) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_DnaSeqMutation_Exome";
CREATE TABLE omicsoft."CCLE_DnaSeqMutation_Exome" (
	"SampleIndex" INTEGER NOT NULL,
	"MutationIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"Mutation" VARCHAR(136) NOT NULL,
	"Coverage" VARCHAR(4) NOT NULL,
	"Frequency" NUMBER(7,3)
);


DROP TABLE IF EXISTS omicsoft."CCLE_DnaSeqMutation";
CREATE TABLE omicsoft."CCLE_DnaSeqMutation" (
	"SampleIndex" INTEGER NOT NULL,
	"MutationIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"Mutation" VARCHAR(33) NOT NULL,
	"Coverage" INTEGER NOT NULL,
	"Frequency" NUMBER(7,3) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_ExonAnnotation";
CREATE TABLE omicsoft."CCLE_ExonAnnotation" (
	"ExonIndex" INTEGER NOT NULL,
	"ExonID" VARCHAR(22) NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"GeneID" VARCHAR(36) NOT NULL,
	"GeneName" VARCHAR(36) NOT NULL,
	"Transcripts" VARCHAR(758) NOT NULL,
	"IsUnique" BOOLEAN NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_ExonData";
CREATE TABLE omicsoft."CCLE_ExonData" (
	"SampleIndex" INTEGER NOT NULL,
	"ExonIndex" INTEGER NOT NULL,
	"ReadCount" NUMBER(19,9) NOT NULL,
	"Rpkm" NUMBER(19,10) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_ExonJunctionAnnotation";
CREATE TABLE omicsoft."CCLE_ExonJunctionAnnotation" (
	"ExonJunctionIndex" INTEGER NOT NULL,
	"ExonJunctionID" VARCHAR(22) NOT NULL,
	"Strand" VARCHAR(1) NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"IntronPosition" INTEGER NOT NULL,
	"IntronSize" INTEGER NOT NULL,
	"Type" VARCHAR(5) NOT NULL,
	"KnownGene" VARCHAR(443),
	"KnownTranscript" VARCHAR(2441),
	"KnownExonNumber" VARCHAR(625),
	"InferredName" VARCHAR(50) NOT NULL,
	"FrameShift" NUMBER(5,1),
	"FrameShiftType" NUMBER(5,1)
);


DROP TABLE IF EXISTS omicsoft."CCLE_ExonJunctionData";
CREATE TABLE omicsoft."CCLE_ExonJunctionData" (
	"SampleIndex" INTEGER NOT NULL,
	"ExonJunctionIndex" INTEGER NOT NULL,
	"ReadCount" INTEGER NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_ExpressionProbes";
CREATE TABLE omicsoft."CCLE_ExpressionProbes" (
	"PlatformIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"ProbeIndex" INTEGER NOT NULL,
	"PlatformName" VARCHAR(25) NOT NULL,
	"ProbeName" VARCHAR(12) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_FusionAnnotation";
CREATE TABLE omicsoft."CCLE_FusionAnnotation" (
	"FusionIndex" INTEGER NOT NULL,
	"FusionID" VARCHAR(29) NOT NULL,
	"Strand" VARCHAR(2) NOT NULL,
	"Chromosome1" VARCHAR(2) NOT NULL,
	"Position1" INTEGER NOT NULL,
	"Chromosome2" VARCHAR(2) NOT NULL,
	"Position2" INTEGER NOT NULL,
	"KnownGene1" VARCHAR(350) NOT NULL,
	"KnownTranscript1" VARCHAR(1308) NOT NULL,
	"KnownExonNumber1" VARCHAR(351) NOT NULL,
	"KnownTranscriptStrand1" VARCHAR(119) NOT NULL,
	"KnownGene2" VARCHAR(350) NOT NULL,
	"KnownTranscript2" VARCHAR(1308) NOT NULL,
	"KnownExonNumber2" VARCHAR(351) NOT NULL,
	"KnownTranscriptStrand2" VARCHAR(119) NOT NULL,
	"FusionGene" VARCHAR(358) NOT NULL,
	"SplicePattern" VARCHAR(5) NOT NULL,
	"SplicePatternClass" VARCHAR(23) NOT NULL,
	"FrameShift" VARCHAR(7) NOT NULL,
	"FrameShiftClass" VARCHAR(10) NOT NULL,
	"Distance" INTEGER NOT NULL,
	"OnExonBoundary" VARCHAR(6) NOT NULL,
	"Filter" VARCHAR(15)
);


DROP TABLE IF EXISTS omicsoft."CCLE_FusionData";
CREATE TABLE omicsoft."CCLE_FusionData" (
	"SampleIndex" INTEGER NOT NULL,
	"FusionIndex" INTEGER NOT NULL,
	"GeneID1" VARCHAR(22) NOT NULL,
	"GeneID2" VARCHAR(36) NOT NULL,
	"UniqueCuttingPosition" INTEGER NOT NULL,
	"SeedCount" INTEGER NOT NULL,
	"RescuedCount" INTEGER NOT NULL,
	"UniqueRead1Position" INTEGER NOT NULL,
	"UniqueRead2Position" INTEGER NOT NULL,
	"PairedEndCount" INTEGER NOT NULL,
	"SingleEnd.RPKM" NUMBER(11,4) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_GeneAnnotation";
CREATE TABLE omicsoft."CCLE_GeneAnnotation" (
	"GeneIndex" INTEGER NOT NULL,
	"GeneID" VARCHAR(36) NOT NULL,
	"EntrezID" NUMBER(13,1),
	"GeneName" VARCHAR(36) NOT NULL,
	"TranscriptNumber" INTEGER NOT NULL,
	"Strand" VARCHAR(1) NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"ExonLength" INTEGER NOT NULL,
	"Source" VARCHAR(16) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_GeneLevelExpression";
CREATE TABLE omicsoft."CCLE_GeneLevelExpression" (
	"SampleIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Value" NUMBER(9,4) NOT NULL
);

DROP TABLE IF EXISTS omicsoft."CCLE_Mutations_Canonical";
CREATE TABLE omicsoft."CCLE_Mutations_Canonical" (
	"MutationIndex" INTEGER NOT NULL,
	"MutationID" VARCHAR(260) NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Position" INTEGER NOT NULL,
	"ReferenceAllele" VARCHAR(1) NOT NULL,
	"MutationAllele" VARCHAR(250) NOT NULL,
	"GeneID" VARCHAR(83) NOT NULL,
	"GeneName" VARCHAR(65) NOT NULL,
	"dbSNPID" VARCHAR(35),
	"dbSNPCategory" VARCHAR(21),
	"AnnotationType" VARCHAR(61),
	"AAPosition" VARCHAR(14),
	"AAChange" VARCHAR(34),
	"AAMutation" VARCHAR(36),
	"AAFrame" VARCHAR(12),
	"TranscriptID" VARCHAR(83),
	"TranscriptName" VARCHAR(85),
	"TranscriptStrand" VARCHAR(13),
	"DistanceTo5Prime" VARCHAR(31) NOT NULL,
	"DistanceTo3Prime" VARCHAR(27) NOT NULL,
	"DistanceToExonBoundary" VARCHAR(25) NOT NULL,
	"Uniprot_acc" VARCHAR(730),
	"Uniprot_id" VARCHAR(168),
	"Ensembl_geneid" VARCHAR(746),
	"SIFT_score" NUMBER(7,3),
	"SIFT_pred" VARCHAR(13),
	"Polyphen2_HDIV_score" NUMBER(7,3),
	"Polyphen2_HDIV_pred" VARCHAR(13),
	"Polyphen2_HVAR_score" NUMBER(7,3),
	"Polyphen2_HVAR_pred" VARCHAR(13),
	"Cosmic.ID" VARCHAR(71),
	"Cosmic.Gene" VARCHAR(121),
	"Cosmic.Strand" VARCHAR(11),
	"Cosmic.CDS" VARCHAR(59),
	"Cosmic.AA" VARCHAR(53),
	"Cosmic.CNT" VARCHAR(11),
	"1000Genome.AlleleFrequency" NUMBER(7,3),
	"1000Genome.AFR.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.AMR.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.EAS.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.EUR.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.SAS.AlleleFrequency.v5" NUMBER(7,3),
	"ucscGenePfam" VARCHAR(223)
);

DROP TABLE IF EXISTS omicsoft."CCLE_Mutations";
CREATE TABLE omicsoft."CCLE_Mutations" (
	"MutationIndex" INTEGER NOT NULL,
	"MutationID" VARCHAR(260) NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Position" INTEGER NOT NULL,
	"ReferenceAllele" VARCHAR(1) NOT NULL,
	"MutationAllele" VARCHAR(250) NOT NULL,
	"GeneID" VARCHAR(347) NOT NULL,
	"GeneName" VARCHAR(347) NOT NULL,
	"dbSNPID" VARCHAR(35),
	"dbSNPCategory" VARCHAR(21),
	"AnnotationType" VARCHAR(1128) NOT NULL,
	"AAPosition" VARCHAR(317) NOT NULL,
	"AAChange" VARCHAR(390),
	"AAMutation" VARCHAR(432),
	"AAFrame" VARCHAR(274),
	"TranscriptID" VARCHAR(838) NOT NULL,
	"TranscriptName" VARCHAR(838) NOT NULL,
	"TranscriptStrand" VARCHAR(124) NOT NULL,
	"DistanceTo5Prime" VARCHAR(418) NOT NULL,
	"DistanceTo3Prime" VARCHAR(418) NOT NULL,
	"DistanceToExonBoundary" VARCHAR(348) NOT NULL,
	"Uniprot_acc" VARCHAR(25252),
	"Uniprot_id" VARCHAR(6622),
	"Ensembl_geneid" VARCHAR(2886),
	"SIFT_score" NUMBER(7,3),
	"SIFT_pred" VARCHAR(13),
	"Polyphen2_HDIV_score" NUMBER(7,3),
	"Polyphen2_HDIV_pred" VARCHAR(13),
	"Polyphen2_HVAR_score" NUMBER(7,3),
	"Polyphen2_HVAR_pred" VARCHAR(13),
	"Cosmic.ID" VARCHAR(71),
	"Cosmic.Gene" VARCHAR(121),
	"Cosmic.Strand" VARCHAR(11),
	"Cosmic.CDS" VARCHAR(59),
	"Cosmic.AA" VARCHAR(53),
	"Cosmic.CNT" VARCHAR(11),
	"1000Genome.AlleleFrequency" NUMBER(7,3),
	"1000Genome.AFR.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.AMR.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.EAS.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.EUR.AlleleFrequency.v5" NUMBER(7,3),
	"1000Genome.SAS.AlleleFrequency.v5" NUMBER(7,3),
	"ucscGenePfam" VARCHAR(223)
);

DROP TABLE IF EXISTS omicsoft."CCLE_RnaSeqMutation";
CREATE TABLE omicsoft."CCLE_RnaSeqMutation" (
	"SampleIndex" INTEGER NOT NULL,
	"MutationIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"Mutation" VARCHAR(12) NOT NULL,
	"Coverage" INTEGER NOT NULL,
	"Frequency" NUMBER(7,3) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_RnaSeqQCTable";
CREATE TABLE omicsoft."CCLE_RnaSeqQCTable" (
	"SampleIndex" INTEGER NOT NULL,
	"AttributeName" VARCHAR(57) NOT NULL,
	"AttributeType" VARCHAR(7) NOT NULL,
	"AttributeValue" VARCHAR(46) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_Samples";
CREATE TABLE omicsoft."CCLE_Samples" (
	"SampleIndex" INTEGER NOT NULL,
	"SampleID" VARCHAR(46) NOT NULL,
	"SubjectID" VARCHAR(46) NOT NULL,
	"CellLineName" VARCHAR(20) NOT NULL,
	"Primary Site" VARCHAR(34) NOT NULL,
	"Histology" VARCHAR(57) NOT NULL,
	"Histology Subtype" VARCHAR(57) NOT NULL,
	"Cell Line Aliases" VARCHAR(24) NOT NULL,
	"Land Sample Type" VARCHAR(17) NOT NULL,
	"Land Tissue" VARCHAR(47) NOT NULL,
	"Gender" VARCHAR(1) NOT NULL,
	"Notes" VARCHAR(113) NOT NULL,
	"Source" VARCHAR(23) NOT NULL,
	"Oncomap" VARCHAR(3) NOT NULL,
	"Hybrid Capture Sequencing" VARCHAR(3) NOT NULL,
	"BamFileName" VARCHAR(39) NOT NULL,
	"Tumor Or Normal" VARCHAR(6) NOT NULL,
	"RNASeq Mapped Read Count" VARCHAR(14) NOT NULL,
	"RNASeq Mapping Rate [Percent]" VARCHAR(7) NOT NULL,
	"BamFileName_DnaSeq_Exome" VARCHAR(50) NOT NULL,
	"HLA A1 [RNA]" VARCHAR(7) NOT NULL,
	"HLA A2 [RNA]" VARCHAR(7) NOT NULL,
	"HLA B1 [RNA]" VARCHAR(7) NOT NULL,
	"HLA B2 [RNA]" VARCHAR(7) NOT NULL,
	"HLA C1 [RNA]" VARCHAR(7) NOT NULL,
	"HLA C2 [RNA]" VARCHAR(7) NOT NULL,
	"HLA A [RNA]" VARCHAR(15) NOT NULL,
	"HLA B [RNA]" VARCHAR(15) NOT NULL,
	"HLA C [RNA]" VARCHAR(15) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_TranscriptAnnotation";
CREATE TABLE omicsoft."CCLE_TranscriptAnnotation" (
	"TranscriptIndex" INTEGER NOT NULL,
	"TranscriptID" VARCHAR(13) NOT NULL,
	"TranscriptName" VARCHAR(17) NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"GeneID" VARCHAR(36) NOT NULL,
	"GeneName" VARCHAR(36) NOT NULL,
	"ExonNumber" INTEGER NOT NULL,
	"Strand" VARCHAR(1) NOT NULL,
	"Chromosome" VARCHAR(2) NOT NULL,
	"Start" INTEGER NOT NULL,
	"End" INTEGER NOT NULL,
	"ExonLength" INTEGER NOT NULL,
	"Source" VARCHAR(16) NOT NULL,
	"RefSeqID" VARCHAR(12)
);


DROP TABLE IF EXISTS omicsoft."CCLE_GeneFPKM";
CREATE TABLE omicsoft."CCLE_GeneFPKM" (
	"SampleIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"FPKM" NUMBER(13,4) NOT NULL,
	"Count" NUMBER(14,4) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_TranscriptFPKM";
CREATE TABLE omicsoft."CCLE_TranscriptFPKM" (
	"SampleIndex" INTEGER NOT NULL,
	"TranscriptIndex" INTEGER NOT NULL,
	"FPKM" NUMBER(12,4) NOT NULL,
	"Count" NUMBER(14,4) NOT NULL
);


DROP TABLE IF EXISTS omicsoft."CCLE_ProbeLevelExpression";
CREATE TABLE omicsoft."CCLE_ProbeLevelExpression" (
	"SampleIndex" INTEGER NOT NULL,
	"GeneIndex" INTEGER NOT NULL,
	"PlatformIndex" INTEGER NOT NULL,
	"ProbeIndex" INTEGER NOT NULL,
	"Expression" NUMBER(9,4) NOT NULL,
	"Expression2" NUMBER(9,4) NOT NULL,
	"Percentile" NUMBER(10,4) NOT NULL,
	"DetectionPValue" NUMBER(8,4) NOT NULL
);


